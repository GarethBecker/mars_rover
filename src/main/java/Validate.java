public abstract class Validate {
    public static boolean isInteger(String test) {
        try {
            Integer.parseInt(test);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isCompassPoint(char point) {
        return (point == 'N' || point == 'E' || point == 'S' || point == 'W');
    }

    public static boolean notEmptyString(String string) {
        return string != null && string.length() > 0;
    }

    public static boolean validateIntStringArray(String[] array) {
        for (String test : array) {
            if (!isInteger(test)) {
                return false;
            }
        }
        return true;
    }

    public static boolean validateGrid(int[] grid) {
        return grid != null && grid.length == 2 && grid[0] != 0 && grid[1] != 0;
    }

    public static boolean validateStartPoint(String[] startPoint) {
        return startPoint != null &&
                startPoint.length == 3 &&
                isInteger(startPoint[0]) &&
                isInteger(startPoint[1]) &&
                isCompassPoint(startPoint[2].charAt(0));
    }

    public static boolean validateCommandString(String commandString) {
        return commandString.matches("[MRL]+");
    }
}
