import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class DataHandling {

    static int[] stringArrayToIntArray(String[] stringArray) {
        if (!Validate.validateIntStringArray(stringArray)) {
            return new int[] {};
        }
        int[] intArray = new int[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            intArray[i] = Integer.parseInt(stringArray[i]);
        }

        return intArray;
    }

    static ArrayList<String> readingDataFile(String path) throws IOException {
        ArrayList<String> inputLines = new ArrayList<>();

        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            inputLines.add(currentLine);
        }
        return inputLines;
    }
}
