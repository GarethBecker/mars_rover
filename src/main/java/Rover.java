public class Rover {
    private static final int NORTH = 0, EAST = 1, SOUTH = 2, WEST = 3;

    private int x;
    private int y;
    private int boundaryX;
    private int boundaryY;
    private int facing;

    public Rover(int x, int y, int boundaryX, int boundaryY, char facingChar) {
        this.x = x;
        this.y = y;
        this.boundaryX = boundaryX;
        this.boundaryY = boundaryY;
        switch (facingChar) {
            case 'N':
                facing = NORTH;
                break;
            case 'E':
                facing = EAST;
                break;
            case 'S':
                facing = SOUTH;
                break;
            case 'W':
                facing = WEST;
        }
    }

    public void move() {
        switch (facing) {
            case NORTH:
                if (y < boundaryY || (boundaryY < 0 && y < 0)) {
                    y++;
                }
                break;
            case SOUTH:
                if (y > boundaryY || (boundaryY > 0 && y > 0)) {
                    y--;
                }
                break;
            case EAST:
                if (x < boundaryX || (boundaryX < 0 && x < 0)) {
                    x++;
                }
                break;
            case WEST:
                if (x > boundaryX || (boundaryX > 0 && x > 0)) {
                    x--;
                }
                break;
        }
    }

    public void rotate(char face) {
        if (face == 'R') {
            if (facing < WEST) facing++;
            else facing = NORTH;
        } else {
            if (facing > NORTH) facing--;
            else facing = WEST;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getFacing() {
        return facing;
    }

    public char getFacingChar() {
        char out = ' ';
        switch (facing) {
            case NORTH:
                out = 'N';
                break;
            case SOUTH:
                out = 'S';
                break;
            case EAST:
                out = 'E';
                break;
            case WEST:
                out = 'W';
                break;
        }
        return out;
    }

    @Override
    public String toString() {
        return String.format("%d %d %c", getX(), getY(), getFacingChar());
    }
}
