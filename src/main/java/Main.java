import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String fileName = "data.txt";

        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                String arg = args[i];
                if (arg.matches("-p")) {
                    if (args.length - 1 <= (i + 1)) {
                        fileName = args[i + 1];
                    } else {
                        System.out.println("Cannot invalid path value, skipping and using default. (data.txt)");
                    }
                }
            }
        }

        try {
            ArrayList<String> inputLines = DataHandling.readingDataFile(fileName);

            String[] gridStringPoints = inputLines.get(0).split(" ");
            if (!Validate.validateIntStringArray(gridStringPoints)) {
                System.out.println("The grid contains invalid points \nPlease only use numbers for your points");
                return;
            }

            int[] gridPoints = DataHandling.stringArrayToIntArray(gridStringPoints);
            if (!Validate.validateGrid(gridPoints)) {
                System.out.println("The grid contains invalid points \nPlease remember not to use 0 for either point");
                return;
            }

            String[] startingPoints = inputLines.get(1).toUpperCase().split(" ");
            if (!Validate.validateStartPoint(startingPoints)) {
                System.out.println("The starting point is invalid \nPlease use numbers for the points and N/E/S/W for the direction");
                return;
            }

            String commandString = inputLines.get(2).toUpperCase();
            if (!Validate.validateCommandString(commandString)) {
                System.out.println("The command string is invalid \nPlease only use the commands M/R/L");
                return;
            }

            Rover rover = new Rover(
                    Integer.parseInt(startingPoints[0]),
                    Integer.parseInt(startingPoints[1]),
                    gridPoints[0],
                    gridPoints[1],
                    startingPoints[2].charAt(0)
            );

            char[] commandChar = commandString.toCharArray();

            for (char dir : commandChar) {
                if (dir == 'M')
                    rover.move();
                else
                    rover.rotate(dir);
            }

            System.out.println(rover.toString());
        } catch (IOException e) {
            System.out.println("Couldn't find file");
            e.printStackTrace();
        }
    }
}
