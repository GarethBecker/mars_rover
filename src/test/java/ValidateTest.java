import org.junit.Test;

import static org.junit.Assert.*;

public class ValidateTest {
    @Test
    public void isIntegerValid() {
        assertTrue(Validate.isInteger("1"));
        assertTrue(Validate.isInteger("-1"));
        assertTrue(Validate.isInteger("0"));
    }

    @Test
    public void isIntegerInValid() {
        assertFalse(Validate.isInteger("abc"));
        assertFalse(Validate.isInteger("12a"));
    }

    @Test
    public void isCompassPointValid() {
        assertTrue(Validate.isCompassPoint('N'));
        assertTrue(Validate.isCompassPoint('E'));
        assertTrue(Validate.isCompassPoint('S'));
        assertTrue(Validate.isCompassPoint('W'));
    }

    @Test
    public void isCompassPointsInvalid() {
        assertFalse(Validate.isCompassPoint('f'));
        // Checking that the char is specifically uppercase because that is what is used throughout
        assertFalse(Validate.isCompassPoint('n'));
        assertFalse(Validate.isCompassPoint('Q'));
    }

    @Test
    public void isNotEmptyStringValid() {
        assertTrue(Validate.notEmptyString("testing things"));
        assertTrue(Validate.notEmptyString("t"));
        assertTrue(Validate.notEmptyString("asdf"));
    }

    @Test
    public void validateIntStringArray() {
        assertTrue(Validate.validateIntStringArray(new String[] {"2", "3", "77"}));
        assertFalse(Validate.validateIntStringArray(new String[] {"4", "a", "b", "8"}));
    }

    @Test
    public void isNotEmptyStringInvalid() {
        assertFalse(Validate.notEmptyString(""));
        assertFalse(Validate.notEmptyString(null));
    }

    @Test
    public void validateGridValid() {
        assertTrue(Validate.validateGrid(new int[] {10, 10}));
        assertTrue(Validate.validateGrid(new int[] {10, -10}));
        assertTrue(Validate.validateGrid(new int[] {-10, -10}));
        assertTrue(Validate.validateGrid(new int[] {-10, 10}));
    }

    @Test
    public void validateGridInvalid() {
        assertFalse(Validate.validateGrid(new int[] {0, 0}));
        assertFalse(Validate.validateGrid(new int[] {89}));
        assertFalse(Validate.validateGrid(new int[] {89, 90, 90}));
        assertFalse(Validate.validateGrid(null));
    }

    @Test
    public void validateStartPointValid() {
        assertTrue(Validate.validateStartPoint(new String[] {"50", "20", "N"}));
        assertTrue(Validate.validateStartPoint(new String[] {"50", "20", "E"}));
        assertTrue(Validate.validateStartPoint(new String[] {"50", "20", "S"}));
        assertTrue(Validate.validateStartPoint(new String[] {"50", "20", "W"}));
        assertTrue(Validate.validateStartPoint(new String[] {"50", "-20", "W"}));
        assertTrue(Validate.validateStartPoint(new String[] {"-50", "-20", "W"}));
        assertTrue(Validate.validateStartPoint(new String[] {"-50", "20", "W"}));
    }

    @Test
    public void validateStartPointInvalid() {
        assertFalse(Validate.validateStartPoint(new String[] {"0", "0", "Q"}));
    }

    @Test
    public void validateCommandString() {
        assertTrue(Validate.validateCommandString("MMMRRRLLL"));
        assertTrue(Validate.validateCommandString("RRLL"));
        assertFalse(Validate.validateCommandString("RTOMML"));
    }
}