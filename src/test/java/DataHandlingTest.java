import org.junit.Test;

import static org.junit.Assert.*;

public class DataHandlingTest {

    @Test
    public void stringArrayToIntArray() {
        assertArrayEquals(new int[] {0, 2, 1}, DataHandling.stringArrayToIntArray(new String[] {"0", "2", "1"}));
        assertArrayEquals(new int[] {}, DataHandling.stringArrayToIntArray(new String[] {"0", "a", "22"}));
    }

}