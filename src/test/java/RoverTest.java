import org.junit.Test;

import static org.junit.Assert.*;

public class RoverTest {
    @Test
    public void basicMove() {
        Rover rover = new Rover(1, 2, 8, 10, 'E');
        rover.move();
        rover.rotate('L');
        rover.move();

        assertEquals("2 3 N", rover.toString());
    }

    @Test
    public void againstBoundsPositiveY() {
        Rover rover = new Rover(0, 20, 8, 30, 'N');
        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("0 30 N", rover.toString());
    }

    @Test
    public void againstBoundsNegativeY() {
        Rover rover = new Rover(0, 0, 8, -20, 'S');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("0 -20 S", rover.toString());
    }

    @Test
    public void againstBounds0PositiveY() {
        Rover rover = new Rover(0, 30, 8, 30, 'S');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals(rover.toString(), "0 0 S");
    }

    @Test
    public void againstBounds0NegativeY() {
        Rover rover = new Rover(0, -30, 8, -30, 'N');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals(rover.toString(), "0 0 N");
    }

    @Test
    public void againstBoundsPositiveX() {
        Rover rover = new Rover(0, 0, 30, 8, 'E');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("30 0 E", rover.toString());
    }

    @Test
    public void againstBoundsNegativeX() {
        Rover rover = new Rover(0, 0, -30, 8, 'W');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("-30 0 W", rover.toString());
    }

    @Test
    public void againstBound0PositiveX() {
        Rover rover = new Rover(30, 0, 30, 8, 'W');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("0 0 W", rover.toString());
    }

    @Test
    public void againstBound0NegativeX() {
        Rover rover = new Rover(-30, 0, -30, 8, 'E');

        for (int i = 0; i < 50; i++) {
            rover.move();
        }

        assertEquals("0 0 E", rover.toString());
    }

    @Test
    public void moveFrom0North() {
        Rover rover = new Rover(0, 0, 20, 8, 'N');
        rover.move();
        rover.move();

        assertEquals("0 2 N", rover.toString());
    }

    @Test
    public void moveFrom0South() {
        Rover rover = new Rover(0, 0, -20, -8, 'S');
        rover.move();
        rover.move();

        assertEquals("0 -2 S", rover.toString());
    }

    @Test
    public void moveFrom0East() {
        Rover rover = new Rover(0, 0, 20, 8, 'E');
        rover.move();
        rover.move();

        assertEquals("2 0 E", rover.toString());
    }

    @Test
    public void moveFrom0West() {
        Rover rover = new Rover(0, 0, -20, -8, 'W');
        rover.move();
        rover.move();

        assertEquals("-2 0 W", rover.toString());
    }
}