The program itself is easy to use. You can either put a "data.txt" file into the root folder and let it read that, or you can set the path using "-p <insert file path>".
The file has to have the format of

The first line must be the boundry coordinates
<BoundaryX><space><BoundaryY>

The second line must be the starting point
<StartingX><space><StartingY><space><Direction>

The final line must be the list of commands, note the commands can only be M/R/L with no spaces.
<CommandString>


When all is said and done the file should look like
8 10
1 2 E //Note the space between the one and the two
MMLMRMMRRMML


// Design choices
The biggest decision that I had to make is the file structure. The entire program could have been written in a single file. The problem with that is that it makes the needlessly difficult to read and incredibly difficult to test. So because of that I moved all of the methods for handling data and validation into there own class. This meant that all the code for these specific things are in one specific place. This also meant that testing was easy because I could test the entire class with a single test class.

The reason that the DataHandling and Validate classes are static is simply because I didn't need actual initiations for them because there is nothing being stored in them.

The linear file structure is because of the sole reason that there are 4 files. Normally it would be far better to separate these files into there own directories. EG: Having Rover in a directory called models and having DataHandling in a utilities directory. But because there aren't many files. A simple linear structure works fine.


// Ensuring the codes correctness
Simply using unit test's on every function meant that everything got a relatively robust test before pressing run.
Only using unit test's wouldn't work if this program was bigger, the only reason these are accurate is because they cover a very large portion of the of the code base.
